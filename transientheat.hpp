// This file is part of the nd-mlhp-examples project. License: See LICENSE

#include "mlhp/core.hpp"

#include <iomanip>

namespace mlhp::linear_am
{

template<size_t D>
auto timeSteppingRefinement( std::array<double, D> current )
{
    auto refine = [=]( double radius1, double radius2, 
                       double level1, double level2 )
    {
        double scaleLeft = 2.2;
        double scaleRight = 1.6;

        return refineWithLevelFunction<D>( [=]( std::array<double, D> xyz )
        {
            auto dx = array::subtract( xyz, current );

            // Rescale in x
            dx[0] = dx[0] < 0 ? dx[0] / scaleLeft : dx[0] / scaleRight;

            // Compute and scale distance
            double d = std::min( ( spatial::norm( dx ) - radius1 ) / ( radius2 - radius1 ), 1.0 );

            double level = d >= 0.0 ? d * level2 + ( 1 - d ) * level1 : 0.0;

            return static_cast<RefinementLevel>( level );
        } );
    };

    return refinementOr( refine( 0.1, 0.02, 0, 3 ),
                         refine( 0.015, 0.005, 3, 5 ) );
}

template<typename AnsatzSpace, size_t D>
auto discretize( std::array<size_t, D> nelements,
                 std::array<double, D> lengths,
                 std::array<double, D> sourceCenter,
                 auto grading )
{
    auto grid0 = makeRefinedGrid( nelements, lengths );

    grid0->refine( timeSteppingRefinement<D>( sourceCenter ) );

    return makeHpBasis<AnsatzSpace>( grid0, grading );
}

template<size_t D>
auto makeIntegrand( const std::vector<double>& dofs0,
                    const SpatialFunction<D + 1>& source,
                    double capacity, double conductivity, 
                    double theta, double time0, double time1  )
{
    auto capacityFunction = spatial::constantFunction<D>( capacity );
    auto conductivityFunction = spatial::constantFunction<D>( conductivity );

    return TransientPoissonIntegrand<D>( capacityFunction, conductivityFunction,
        spatial::peelLast( source ), dofs0, { time0, time1 }, theta );
}

template<size_t D>
auto writeSolutionToVtu( const MultilevelHpBasis<D>& basis,
                         const std::vector<double>& dofs,
                         size_t iStep, size_t interval, double time,
                         const SpatialFunction<D + 1>& solution,
                         const SpatialFunction<D + 1>& source )
{
    if( iStep % interval == 0 )
    {
        std::vector<FunctorElementPostprocessor<D>> postprocessors;

        postprocessors.push_back( solutionPostprocessor<D>( dofs, 1 ) );

        // Postprocess analytical solution
        postprocessors.push_back( spatialFunctionPostprocessor<D>(
            spatial::sliceLast( solution, time ), "Solution" ) );

        // Postprocess source
        postprocessors.push_back( spatialFunctionPostprocessor<D>(
            spatial::sliceLast( solution, time ), "Solution" ) );

        // Sub-samples per element
        auto fullName = "outputs/linear_am_" + std::to_string( ( iStep / interval ) + 1 );
        auto nsamples = array::make<size_t, D>( 7 );

        postprocess( nsamples, postprocessors, basis, fullName );
    }
}

struct ErrorIntegralNorms
{
    double numerical( ) { return std::sqrt( numericalSquared ); }
    double analytical( ) { return std::sqrt( analyticalSquared ); }
    double difference( ) { return std::sqrt( differenceSquared ); }
    double relativeDifference( ) { return std::sqrt( differenceSquared / analyticalSquared ); }

    double analyticalSquared = 0, numericalSquared = 0, differenceSquared = 0;
    size_t ndof = 0;
};

template<size_t D>
void addL2ErrorIntegral( const MultilevelHpBasis<D>& basis,
                         const SpatialFunction<D + 1>& solution,
                         const std::vector<double>& dofs,
                         double time, double weight, ErrorIntegralNorms& norms )
{
    auto errorIntegrationOrder = makeIntegrationOrderDeterminor<D>( 1 );
    auto errorIntegrand = L2ErrorIntegrand<D>( dofs, spatial::sliceLast( solution, time ) );
    auto errorsOfStep = integrateScalarFunctionsOnDomain( basis, errorIntegrand, errorIntegrationOrder );

    norms.numericalSquared += weight * errorsOfStep[0];
    norms.analyticalSquared += weight * errorsOfStep[1];
    norms.differenceSquared += weight * errorsOfStep[2];
    norms.ndof += basis.ndof( );
}

} // namespace mlhp::linear_am

