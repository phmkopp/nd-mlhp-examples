This repository contains multi-level _hp_ examples for this paper: P. Kopp, E. Rank, V. Calo and S. Kollmannsberger, "Efficient multi-level _hp_-finite elements in arbitrary dimensions". The preprint of the paper can be found [here](https://arxiv.org/pdf/2106.08214.pdf).

Cloning the project should be done with `--recursive` to clone also all submodules. More information about using the code can be found in the main repository: [https://gitlab.com/phmkopp/mlhp](https://gitlab.com/phmkopp/mlhp). 

The three examples within the repository are built and run in the CI of the project. You can check the [most recent pipeline](https://gitlab.com/phmkopp/nd-mlhp-examples/pipelines/latest) for logs and the [result files](https://gitlab.com/phmkopp/nd-mlhp-examples/-/jobs/artifacts/master/download?job=transientheat-picture) for the `transientheat-picture` example (as artifact). 
