// This file is part of the nd-mlhp-examples project. License: See LICENSE

#include "transientheat.hpp"

int main( )
{
    using namespace mlhp;
    
    static constexpr size_t D = 3;

    auto t0 = utilities::tic( );

    // Physics setup
    double duration = 1.0;
    double sigma = 0.0021;
    double u0 = 25.0;

    double capacity = 1.0;
    auto conductivity = 0.01;

    std::array<double, 3> lengths { 1.0, 0.4, 0.1 };

    std::function path = [=]( double t ) noexcept
    {
        double x = ( 0.6 * t / duration + 0.2 ) * lengths[0];

        return std::array<double, 3>{ x, lengths[1] / 2.0, lengths[2] };
    };

    double energy = 0.484;

    std::function intensity = [=]( double t ) noexcept
    {
        return 2.0 * energy * std::min( t / 0.05, 1.0 );
    };

    SpatialFunction<D + 1> source = solution::amLinearHeatSource( path, intensity, sigma );

    SpatialFunction<D + 1> solution = solution::amLinearHeatSolution( path,
        intensity, capacity, conductivity, sigma, duration / 100, u0 );

    auto initialCondition = spatial::constantFunction<D>( u0 );

    auto boundaries = boundary::allFaces( D );

    boundaries.pop_back( );

    // Discretization parameters
    size_t nsteps = 256;
    double theta = 0.5;

    std::array<size_t, 3> nelements = { 10, 4, 1 };

    PolynomialDegreeVector degrees { 5, 5, 4, 3, 3, 2 };

    auto grading = PerLevelGrading { degrees };

    using AnsatzSpace = TrunkSpace;

    // Change to 1 to postprocess every time step
    size_t vtuInterval = nsteps;

    // Initial discretization
    auto basis0 = linear_am::discretize<AnsatzSpace>( nelements, lengths, path( 0.0 ), grading );

    auto dofs0 = projectOnto( *basis0, initialCondition );

    // Assembly stuff
    auto orderDeterminor = makeIntegrationOrderDeterminor<D>( 1 );
    auto partitioner = NoPartitioner<D> { };
    auto solve = linalg::makeCGSolver( 1e-6 );

    double dt = duration / nsteps;

    //// Error in initial condition 
    //linear_am::ErrorIntegralNorms norms;
    //linear_am::addL2ErrorIntegral( *basis0, solution, dofs0, 0.0, dt / 2.0, norms );

    //// Postprocess initial condition (although zero)
    //linear_am::writeSolutionToVtu( *basis0, dofs0, 0, vtuInterval, 0.0, solution, source );

    // Time stepping
    for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
    {
        double time0 = iTimeStep * dt;
        double time1 = ( iTimeStep + 1 ) * dt;

        // Create discetisation for t^{i+1}
        auto basis1 = linear_am::discretize<AnsatzSpace>( nelements, lengths, path( time1 ), grading );

        std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
        std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;

        // Compute boundary dofs
        auto function = spatial::sliceLast( solution, time1 );

        auto dirichletDofs = boundary::boundaryDofs<D>( function, *basis1, boundaries );

        // Assemble linear system
        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis1, dirichletDofs.first );
        auto rhs = std::vector<double>( matrix.size1( ), 0.0 );

        auto integrand = linear_am::makeIntegrand<D>( dofs0, source, 
                capacity, conductivity, theta, time0, time1 );

        integrateLinearSystemOnDomain( *basis1, *basis0, integrand, 
            matrix, rhs, partitioner, orderDeterminor, dirichletDofs );

        // Solve linear system
        auto dofs1 = boundary::inflate( solve( matrix, rhs ), dirichletDofs );

        //// Vtu postprocessing
        //linear_am::writeSolutionToVtu( *basis1, dofs1, iTimeStep + 1, vtuInterval, time1, solution, source );

        //// Error integral
        //double weight = iTimeStep + 1 < nsteps ? dt : dt / 2.0;
        //linear_am::addL2ErrorIntegral( *basis1, solution, dofs1, time1, weight, norms );

        std::cout << "\tGrid memory: " << basis1->mesh( ).memoryUsage( ) << std::endl;
        std::cout << "\tBasis memory: " << basis1->memoryUsage( ) << std::endl;
        std::cout << "\tMatrix memory: " << matrix.memoryUsage( ) << std::endl;

        // Move discretization for next step
        dofs0 = dofs1;
        basis0 = basis1;
    }

    // Only postprocess final solution
    linear_am::writeSolutionToVtu( *basis0, dofs0, nsteps - 1, 1, duration, solution, source );

    //std::cout << "ndof, || u ||, || u - u^h ||" << std::endl;
    //std::cout << norms.ndof << ", " << std::scientific << std::setprecision( 12 ) 
    //          << norms.analytical( ) << ", "
    //          << norms.relativeDifference( ) << std::endl;

    std::cout << "Runtime: " << utilities::seconds( t0, utilities::tic( ) ) << " seconds.\n";
}
